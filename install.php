﻿<?php
	$page = isset($_GET['pag']) ? $_GET['page'] : '1';
	$prev = $page - 1;
	$next = $page + 1;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Plugin Stylesheets first to ease overrides -->
<link rel="stylesheet" type="text/css" href="plugins/colorpicker/colorpicker.css" media="screen">
<link rel="stylesheet" type="text/css" href="custom-plugins/wizard/wizard.css" media="screen">


<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/fonts/ptsans/stylesheet.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/fonts/icomoon/style.css" media="screen">

<link rel="stylesheet" type="text/css" href="css/mws-style.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/icons/icol16.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/icons/icol32.css" media="screen">

<!-- Demo Stylesheet -->
<link rel="stylesheet" type="text/css" href="css/demo.css" media="screen">

<!-- jQuery-UI Stylesheet -->
<link rel="stylesheet" type="text/css" href="jui/css/jquery.ui.all.css" media="screen">
<link rel="stylesheet" type="text/css" href="jui/jquery-ui.custom.css" media="screen">

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="css/mws-theme.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/themer.css" media="screen">

<title>Install SMS Gateway | Nuansa Corp V1.0</title>

</head>

<body>

	<!-- Header -->
	<div id="mws-header" class="clearfix">
    
    	<!-- Logo Container -->
    	<div id="mws-logo-container">
        
        	<!-- Logo Wrapper, images put within this wrapper will always be vertically centered -->
        	<div id="mws-logo-wrap">
            	<img src="images/mws-logo.png" alt="mws admin">
			</div>
        </div>
        
    </div>
    
    <!-- Start Main Wrapper -->
    <div id="mws-wrapper">
        
        	<!-- Inner Container Start -->
            <div class="container">
                <div class="clearfix">&nbsp;</div>
                <!-- Panels Start -->

            	<div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-magic"></i> Install SMS Gateway | Nuansa Corp V1.0</span>
                    </div>
                    
                    <div class="mws-panel-body no-padding">
                        <div class="wizard-nav wizard-nav-horizontal">
                        	<ul>
                            	<li <?php if ($page == '1'): ?>class="current"<?php endif; ?>>
                                	<span><a href="?step=1">1. Setting Database</a></span>
                                </li>
                                <li <?php if ($page == '2'): ?>class="current"<?php endif; ?>>
                               		<span><a href="?step=2">2. Setting Phone/Modem</a></span>
                               	</li>
                                <li <?php if ($page == '3'): ?>class="current"<?php endif; ?>>
                                	<span><a href="?step=3">3. Test Kirim SMS</a></span>
                                </li>
                                <li <?php if ($page == '4'): ?>class="current"<?php endif; ?>>
                                	<span><a href="?step=4">4. Test Menerima SMS</a></span>
                                </li>
                                <li <?php if ($page == '5'): ?>class="current"<?php endif; ?>>
                                	<span><a href="?step=5">5. Data Pribadi dan Account</a></span>
                                </li>
                           	</ul>
                      	</div>
                        <div class="mws-form wzd-validate wizard-form wizard-form-horizontal" novalidate="novalidate">
                        	<fieldset class="wizard-step mws-form-inline" style="display: block;">
                        		<?php
									switch ($page)
									{
										default: 
											// 404
											break;
										case '1':
										case '2':
										case '3':
										case '4':
										case '5':
											if (file_exists('pages/install/' . $page . '.php')) require_once 'pages/install/' . $page . '.php';
											break;
										
									}
								?>
                        	
                        </div>
                    </div>
                    
                </div>

                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->
                       
            <!-- Footer -->
            <div id="footer-install">
            	Copyright Nuansa Corp 2014. All Rights Reserved.
            </div>
        
    </div>

    <!-- JavaScript Plugins -->
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script src="js/libs/jquery.mousewheel.min.js"></script>
    <script src="js/libs/jquery.placeholder.min.js"></script>
    <script src="custom-plugins/fileinput.js"></script>

    <!-- jQuery-UI Dependent Scripts -->
    <script src="jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="jui/jquery-ui.custom.min.js"></script>
    <script src="jui/js/jquery.ui.touch-punch.js"></script>

    <!-- Plugin Scripts -->
    <script src="plugins/colorpicker/colorpicker-min.js"></script>
    <script src="plugins/validate/jquery.validate-min.js"></script>

    <!-- Wizard Plugin -->
    <script src="custom-plugins/wizard/jquery.form.min.js"></script>

    <!-- Core Script -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/core/mws.js"></script>

</body>
</html>
