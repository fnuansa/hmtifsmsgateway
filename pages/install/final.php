							<form method="post" action="login.php">
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Nama Lengkap <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                    	<input type="hidden" name="opsi" value="submitDataDiri" />
                                        <input type="text" name="namaLengkap" class="required large">
                                    </div>
                                </div>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">username <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="user" class="required large">
                                    </div>
                                </div>
                                <div id class="mws-form-row">
                                    <label class="mws-form-label">Password <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="pass" class="required large">
                                    </div>
                                </div>
                                <div class="mws-form-row">
                                    <label class="mws-form-label">No. Telp <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="text" name="noTelp" class="required large">
                                    </div>
                                </div>
                                <div class="mws-form-row">
                                    <label class="mws-form-label">Email <span class="required">*</span></label>
                                    <div class="mws-form-item">
                                        <input type="email" name="email" class="required email large">
                                    </div>
                                </div>
                                </fieldset>
                             <div class="mws-button-row">
                                <?php if ($prev == '0'): ?>
                                	<button type="button" class="btn" <?php if ($prev == '0'): ?> disabled="disabled"<?php endif; ?>>
                                		Prev
                                    </button>
                               	<?php else: ?>
                               		<a class="btn" href="?step=<?php echo $prev ?>">Prev</a>
                                <?php endif; ?>
                                <?php if ($next == '6'): ?>
                                	<button type="button" class="btn" <?php if ($next == '6'): ?> disabled="disabled"<?php endif; ?>>
                                        Next
                                    </button>
                                <?php else: ?>
                               		<a class="btn" href="?step=<?php echo $next ?>">Next</a>
                                <?php endif; ?>
                                <?php if ($page == '5'): ?>
                                	<input type="submit" name="submit" value="Submit" class="btn btn-primary pull-right">
                                <?php endif; ?>
    						</form>
 