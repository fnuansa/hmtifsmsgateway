<?php
error_reporting(E_ALL ^ E_NOTICE);
// jumlah maksimum modem yg bisa diset
$maxmodem = 2;

include 'pages/install/function.php';

if (isset($_GET['opsi']))
{
if ($_GET['opsi'] == 'simpan')
{
	include "pages/install/db.php";
	$id = str_replace(" ","-",$_POST['modem']);
	$smsdrc = $_POST['smsdrc'];
	$port = strtolower(str_replace(" ","", $_POST['port']));
	$connection = strtolower(str_replace(" ","", $_POST['koneksi']));
	$send = 'yes';
	$receive = 'yes';
	$path = str_replace('install.php?step=2', '', $_SERVER['SCRIPT_FILENAME']);
	$handle = @fopen($smsdrc, "w");
	$text = "[gammu]
# isikan no port di bawah ini
port = ".$port.":
# isikan jenis connection di bawah ini
connection = ".$connection."

[smsd]
service = mysql
logfile = ".$path."log".$smsdrc."
debuglevel = 0
phoneid = ".$id."
commtimeout = 30
sendtimeout = 600
send = ".$send."
receive = ".$receive."
checksecurity = 0
#PIN = 1234

# -----------------------------
# Konfigurasi koneksi ke MySQL
# -----------------------------
pc = localhost

# isikan user untuk akses ke MySQL
user = ".$dbuser."
# isikan password user untuk akses ke MySQL
password = ".$dbpass."
# isikan nama database untuk Gammu
database = ".$dbname."\n";

  fwrite($handle, $text);
  fclose($handle);
  
	$string = "";
	$j = 0;
	for($i=1; $i<=$maxmodem; $i++)
	{
		if (is_file('smsdrc'.$i))
		{
			$handle = @fopen("smsdrc".$i, "r");
			if ($handle) 
			{
				while (!feof($handle)) 
				{
					$buffer = fgets($handle);
					if (substr_count($buffer, 'port = ') > 0)
					{
						$split = explode("port = ", $buffer);
						$port = $split[1];
					}
					if (substr_count($buffer, 'connection = ') > 0)
					{
						$split = explode("connection = ", $buffer);
						$connection = $split[1];
				}
				}
			}		
			fclose($handle);
			if ($j==0) $string .= "[gammu]\nport = ".$port."connection = ".$connection."\n";
			else $string .= "[gammu".($j)."]\nport = ".$port."connection = ".$connection."\n";
			$j++;
		}	
	}
	$handle = @fopen("gammurc", "w");
	fwrite($handle, $string);
	fclose($handle);

}
}

if (isset($_GET['opsi']))
{
if ($_GET['opsi'] == 'del')
{
	$id = $_GET['id'];
	

	if(is_file("logsmsdrc".$id)) unlink("logsmsdrc".$id);
	exec("gammu-smsd -n ".getParam('id', $id)." -k", $hasil);
	exec("gammu-smsd -n ".getParam('id', $id)." -u", $hasil);
	unlink("smsdrc".$id);
	
	$string = "";
	$j = 0;
	for($i=1; $i<=$maxmodem; $i++)
	{
		if (is_file('smsdrc'.$i))
		{
			$handle = @fopen("smsdrc".$i, "r");
			if ($handle) 
			{
				while (!feof($handle)) 
				{
					$buffer = fgets($handle);
					if (substr_count($buffer, 'port = ') > 0)
					{
						$split = explode("port = ", $buffer);
						$port = $split[1];
					}
					if (substr_count($buffer, 'connection = ') > 0)
					{
						$split = explode("connection = ", $buffer);
						$connection = $split[1];
					}
				}
			}		
			fclose($handle);
			if ($j==0) $string .= "[gammu]\nport = ".$port."connection = ".$connection."\n";
			else $string .= "[gammu".($j)."]\nport = ".$port."connection = ".$connection."\n";
			$j++;
		}	
	}
	$handle = @fopen("gammurc", "w");
	fwrite($handle, $string);
	fclose($handle);
	
}
}

for($i=1; $i<=$maxmodem; $i++)
{
	if (is_file('smsdrc'.$i))
	{
		$sum = $i + 1;
	}	
}

if ($sum == 0) $sum = 1;

$nextsmsdrc = "smsdrc".$sum;

?>
							<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>?step=2&opsi=simpan" class="mws-form wzd-validate">

                                <div class="mws-form-message warning">
                                	Masukkan port & jenis koneksi HP atau Modem anda.
                                </div>
                              	<div class="grid_4">
                                    <div id class="mws-form-row">
                                        <label class="mws-form-label">ID Phone / Modem <span class="required">*</span></label>
                                        <div class="mws-form-item">
                                            <input type="text" name="modem" class="required large">
                                            <div class="text-info">Isikan sembarang nama untuk identitas modem Anda, Contoh: Modem 1</div>
                                        </div>
                                    </div>
                                    <div id class="mws-form-row">
                                        <label class="mws-form-label">Port <span class="required">*</span></label>
                                        <div class="mws-form-item">
                                            <input type="text" name="port" class="required large">
                                            <input type="hidden" name="smsdrc" value="<?php echo $nextsmsdrc; ?>">
                                            <div class="text-info">Masukkan nomor port modem/hp.</div>
                                            <div class="text-info">Contoh penulisan: com4 (dengan huruf kecil dan tanpa spasi apa-apa)</div>
                                        </div>
                                    </div>
                                    <div id class="mws-form-row">
                                        <label class="mws-form-label">Koneksi <span class="required">*</span></label>
                                        <div class="mws-form-item">
                                            <select name="koneksi" class="required large">
                                                <option>at115200</option>
                                                <option>at19200</option>
                                                <option>at9600</option>
                                                <option>at</option>
                                            </select>
                                            <div class="text-info">
                                                Pilih jenis connection hp/modem Anda. 
                                                <br>
                                                <a href="jenis_koneksi.xls" target="_blank">Lihat Jenis Connection</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mws-form-row" align="right">
                                        <input type="submit" name="submit1" value="Simpan" class="btn btn-danger">
                                    </div>
                            	</div>
                                <div class="grid_4">
                                	<div id class="mws-form-row">
                                        <?php
											$sum = 0;
											for($i=1; $i<=$maxmodem; $i++)
											{
												if (is_file('smsdrc'.$i))
												{
													$sum++;
												}	
											}
											
											if ($sum == 0) 
											echo "Phone/Modem belum ada";
											else
											{
											echo "<table class='mws-table'>";
											echo "
												<thead>
													<tr>
														<th>ID Phone</th>
														<th>Port</th>
														<th>Connection</th>
														<th width='45%'>Action</th>
													</tr>
												</thead>
												<tbody>
											";
											$count = 0;
											for($i=1; $i<=$maxmodem; $i++)
											{
												if (is_file('smsdrc'.$i))
												{
														$count++;
														$handle = @fopen("smsdrc".$i, "r");
														if ($handle) 
														{
															while (!feof($handle)) 
															{
																$buffer = fgets($handle);
																if (substr_count($buffer, 'port = ') > 0)
																{
																	$split = explode("port = ", $buffer);
																	$port = $split[1];
																}
																if (substr_count($buffer, 'phoneid = ') > 0)
																{
																	$split = explode("phoneid = ", $buffer);
																	$phone = $split[1];
																}
																if (substr_count($buffer, 'connection = ') > 0)
																{
																	$split = explode("connection = ", $buffer);
																	$conn = $split[1];
																}
															}
														}
												
													echo "
														<tr>
															<td>".$phone."</td>
															<td>".$port."</td>
															<td>".$conn."</td>
															<td>
																<a href='".$_SERVER['PHP_SELF']."?step=2&opsi=cek&id=".$count."' class='btn btn-success btn-small'>CEK KONEKSI</a> <br>
																<a href='".$_SERVER['PHP_SELF']."?step=2&opsi=service&id=".$i."' class='btn btn-primary btn-small'>BUAT SERVICE</a> <br>
																<a href='".$_SERVER['PHP_SELF']."?step=2&opsi=del&id=".$i."' class='btn btn-danger btn-small'>HAPUS</a></td> <br>
														</tr>";
													$sum++;
													
												}	
											}
											echo "</tbody></table>";
											echo "<p><b>Penting !!!</b><br>Pastikan sebelum menghapus modem, service Gammu untuk modem tersebut harus dimatikan dahulu</p>";											
												if (isset($_GET['opsi']))
												{
													if ($_GET['opsi'] == 'cek')
													{
														$id = ($_GET['id']-1);
														echo "<p><b>Status Koneksi Phone/Modem ".$_GET['id']."</b></p>";
														echo "<pre>";
														passthru("gammu -s ".$id." -c gammurc identify", $hasil);
														echo "</pre>";
													}
												}
											
												if (isset($_GET['opsi']))
												{
													if ($_GET['opsi'] == 'service')
													{
														$id = $_GET['id'];
														echo "<p><b>Status Service Phone/Modem: ".getParam('id', $id)."</b></p>";
														echo "<pre>";
														exec("gammu-smsd -n ".getParam('id', $id)." -k", $hasil);
														exec("gammu-smsd -n ".getParam('id', $id)." -u", $hasil);
														passthru("gammu-smsd -c smsdrc".$id." -n ".getParam('id', $id)." -i");
														exec("sc config ".getParam('id', $id)." start= demand");
														echo "</pre>";
													}
												}
											}
											
											?>
                                    </div>
                                </div>
                                </fieldset>
                             <div class="mws-button-row">
                                <?php if ($prev == '0'): ?>
                                	<button type="button" class="btn" <?php if ($prev == '0'): ?> disabled="disabled"<?php endif; ?>>
                                		Prev
                                    </button>
                               	<?php else: ?>
                               		<a class="btn" href="?step=<?php echo $prev ?>">Prev</a>
                                <?php endif; ?>
                                <?php if ($next == '6'): ?>
                                	<button type="button" class="btn" <?php if ($next == '6'): ?> disabled="disabled"<?php endif; ?>>
                                        Next
                                    </button>
                                <?php else: ?>
                               		<a class="btn" href="?step=<?php echo $next ?>">Next</a>
                                <?php endif; ?>
                                <button type="button" class="btn btn-primary pull-right" name="install-submit" 
									<?php if ($page != '5'): ?>style="display: none;"<?php endif; ?>>Submit
                                </button>
						</form>