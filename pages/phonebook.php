				<div class="mws-panel grid_4">
                    <div class="mws-panel-header">
                        <span><i class="icon-users"></i> Kontak Group</span>
                    </div>
                    <div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <a href="#" class="btn"><i class="icon-plus"></i> Tambah Group</a>
                                <a href="#" class="btn"><i class="icon-publish"></i> Tambah Kontak Group</a>
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th width="10px;">No</th>
                                    <th>Nama Group</th>
                                    <th width="75px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Dokuminfo</td>
                                    <td>
                                    	<a href="#" class="btn btn-success mws-login-button"><i class="icon-pencil-2"></i></a>
                                        <a href="#" class="btn btn-success mws-login-button"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>      
                </div>
                
                				<div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-user"></i> Kontak</span>
                    </div>
                    <div class="mws-panel-toolbar">
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <a href="#" class="btn"><i class="icon-plus"></i> Tambah Kontak</a>
                            </div>
                        </div>
                    </div>
                    <div class="mws-panel-body">
                        <table class="mws-table">
                            <thead>
                                <tr>
                                    <th width="10px;">No</th>
                                    <th>Nama Kontak</th>
                                    <th>No Telpon</th>
                                    <th width="120px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Fahrizal Nuansa</td>
                                    <td>08530090004</td>
                                    <td>
                                    	<a href="#" class="btn btn-success mws-login-button"><i class="icon-mobile-phone"></i></a>
                                    	<a href="#" class="btn btn-success mws-login-button"><i class="icon-pencil-2"></i></a>
                                        <a href="#" class="btn btn-success mws-login-button"><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>      
                </div>